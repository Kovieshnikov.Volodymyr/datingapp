﻿using System;

namespace DatingApp.API.Extensions
{
    public static class DateTimeExrensions
    {
        public static int CalculateAge(this DateTime dob)
        {
            var today = DateTime.Today;
            var age = today.Year - dob.Year;

            if (dob.Date > today.AddYears(-age))
                --age;

            return age;
        }
    }
}