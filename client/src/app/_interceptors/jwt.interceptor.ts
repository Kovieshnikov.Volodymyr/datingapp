import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import {AccountService} from "../_services/account.service";
import {User} from "../models/user";
import {take} from "rxjs/operators";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  constructor(private accountService: AccountService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let currenrUser: User;

    this.accountService.currentUser$.pipe(take(1)).subscribe(user => currenrUser = user); // because of take(1) no need to unsubscribe
    if (currenrUser) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${currenrUser.token}`
        }
      })
    }
    return next.handle(request);
  }
}
